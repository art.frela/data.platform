CREATE EXTENSION IF NOT EXISTS pgcrypto;
/*******************************************************************************
 * public.users 
 * таблица пользователей
 ******************************************************************************/
create table if not exists public.users (
  id uuid not null default gen_random_uuid(), -- идентификатор
  "login" varchar(255) not null, -- логин пользователя
  email varchar(300) UNIQUE not null, -- электропочта пользователя
  "password" bytea not null, -- хэш пароля пользователя
  creator varchar(128) not null default current_user, -- пользователь, создавший запись
  created_at timestamptz not null default current_timestamp, -- дата-время создания записи
  modifier varchar(128) null, -- пользователь, внесший последние изменения
  modified_at timestamptz null, -- дата-время последних изменений
  constraint users_pk primary key (id)
);

comment on table public.users is 'список пользователей';
comment on column public.users.id is 'уникальный идентификатор пользователя';
comment on column public.users.login is 'уникальное имя пользователя (логин)';
comment on column public.users.email is 'уникальный адрес жлектронной почты пользователя';
comment on column public.users.password is 'хэш пароля пользователя';
comment on column public.users.creator is 'имя пользователя создавшего запись';
comment on column public.users.created_at is 'дата время создания записи';
comment on column public.users.modifier is 'имя пользователя последним изменившего запись';
comment on column public.users.modified_at is 'дата время последнего изменения записи';