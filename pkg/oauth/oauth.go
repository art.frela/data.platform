// Package oauth contains contract for oauth provider behavior
package oauth

import "context"

// ProviderInterface behavior of the oauth provider
type ProviderInterface interface {
	FetchUserEmail(ctx context.Context, code string) (string, error)
	GetAuthURL() string
}
