// Package vk implements of the OAuth Provider interface with vk.com api
package vk

import (
	"context"
	"errors"
	"fmt"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/vk"
)

var (
	// ErrEmptyEmail vk user doesn't have email address
	ErrEmptyEmail error = errors.New("empty email")
)

// OAuthVK implementation of the oauth provider interface for vk.com
type OAuthVK struct {
	appID       string
	appKey      string
	authURL     string
	redirectURL string
}

// NewOAuthVK builder for OAuthVK
func NewOAuthVK(appID, appKey, redirectURL, scope string) *OAuthVK {
	ovk := &OAuthVK{
		appID:  appID,
		appKey: appKey,
		authURL: fmt.Sprintf("%s?client_id=%s&redirect_uri=%s&response_type=code&scope=%s",
			vk.Endpoint.AuthURL, appID, redirectURL, scope),
		redirectURL: redirectURL,
	}
	return ovk
}

// GetAuthURL returns provider specified auth url
func (ovk *OAuthVK) GetAuthURL() string {
	return ovk.authURL
}

// FetchUserEmail extracts user email by vk api
func (ovk *OAuthVK) FetchUserEmail(ctx context.Context, code string) (string, error) {
	if code == "" {
		return "", fmt.Errorf("empty code")
	}
	conf := oauth2.Config{
		ClientID:     ovk.appID,
		ClientSecret: ovk.appKey,
		RedirectURL:  ovk.redirectURL,
		Endpoint:     vk.Endpoint,
	}
	token, err := conf.Exchange(ctx, code)
	if err != nil {
		return "", fmt.Errorf("get access token error: %s", err)
	}
	emailvk, ok := token.Extra("email").(string)
	if !ok {
		return "empty", ErrEmptyEmail
	}

	return emailvk, nil
}
