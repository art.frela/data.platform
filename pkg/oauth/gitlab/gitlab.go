// Package gitlab implements of the OAuth Provider interface with gitlab.com api
package gitlab

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/gitlab"
)

const (
	// UserAPIURL gitlab.com api endpoint
	UserAPIURL = "https://gitlab.com/api/v4/user"
)

// OAuthGitLab implementation of the oauth provider interface for gitlab.com
type OAuthGitLab struct {
	appID       string
	appKey      string
	authURL     string
	redirectURL string
}

// NewOAuthGitLab builder for OAuthGitLab
func NewOAuthGitLab(appID, appKey, redirectURL, scope string) *OAuthGitLab {
	ogl := &OAuthGitLab{
		appID:  appID,
		appKey: appKey,
		authURL: fmt.Sprintf("%s?client_id=%s&redirect_uri=%s&response_type=code&scope=%s",
			gitlab.Endpoint.AuthURL, appID, redirectURL, scope),
		redirectURL: redirectURL,
	}
	return ogl
}

// GetAuthURL returns provider specified auth url
func (ogl *OAuthGitLab) GetAuthURL() string {
	return ogl.authURL
}

// UserInfo gitlab specified user structure
type UserInfo struct {
	Email string `json:"email"`
}

// FetchUserEmail extracts user email by gitlab api
func (ogl *OAuthGitLab) FetchUserEmail(ctx context.Context, code string) (string, error) {
	if code == "" {
		return "", fmt.Errorf("empty code")
	}
	conf := oauth2.Config{
		ClientID:     ogl.appID,
		ClientSecret: ogl.appKey,
		RedirectURL:  ogl.redirectURL,
		Endpoint:     gitlab.Endpoint,
	}
	token, err := conf.Exchange(ctx, code)
	if err != nil {
		return "", fmt.Errorf("get access token error: %s", err)
	}
	ui, err := ogl.requestProvider(ctx, conf, token)
	if err != nil || ui == nil {
		return "", fmt.Errorf("get user info error: %s", err)
	}
	return ui.Email, nil
}

func (ogl *OAuthGitLab) requestProvider(ctx context.Context,
	conf oauth2.Config,
	token *oauth2.Token) (*UserInfo, error) {

	client := conf.Client(ctx, token)
	resp, err := client.Get(UserAPIURL)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	data := &UserInfo{}
	err = json.Unmarshal(body, data)
	return data, err
}
