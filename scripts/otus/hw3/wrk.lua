math.randomseed(os.time())
request = function()
   local k = math.random(0, 100000000)
   local t
   if k > 950 then
      t = "incorrect_admin_token"
   else
      t = "admin_secret_token"
   end

   local url = "/users"
   local headers = {}
   local body = '{\"login\":\"user-'..k..'\",\"email\":\"usermail-'..k..'@domain.com\",\"password\":\"'..t..'\"}'
   headers["Content-Type"] = "application/json"
   --print(body)
   return wrk.format("POST", url, headers, body)
end
