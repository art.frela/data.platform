OTUSHW1 = "registry.gitlab.com/art.frela/data.platform:otushw1"
OTUSHW2 = "registry.gitlab.com/art.frela/data.platform:otushw2.hw2.v0.0.2"


# h - help
h help:
	@echo "h help 	- this help"
	@echo "dockerhw1 	- run docker image build for otus homework 1"
.PHONY: h



# docker build
dockerhw1:
	docker build -f ./build/otus/hw1/Dockerfile -t $(OTUSHW1) .
.PHONY: dockerhw1

dockerhw2:
	docker build -f ./build/otus/hw2/Dockerfile -t $(OTUSHW2) .
.PHONY: dockerhw1

runhw1:
	docker run -d -p 8000:8000 \
	--name=otuswh1 $(OTUSHW1)
.PHONY: runhw1

runhw2:
	docker run -d -p 8000:8000 \
	-v /Users/artem/projects/gitlab.com/art.frela/data.platform/cmd/otus/hw2/otushw2.yaml:/etc/otushw2/otushw2.yaml \
	--name=otushw2 $(OTUSHW2)
.PHONY: runhw1

#	-e OTUSHW2_DB_PROVIDER=postgres \
#	-e OTUSHW2_DB_URL=postgres://postgres:test@10.11.12.1:8081/users?sslmode=disable \
#