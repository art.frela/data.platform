# Homework #1 Основы работы с Kubernetes (часть 2)

Создать минимальный сервис, который  
1) отвечает на порту 8000  
2) имеет http-метод  
`GET /health/`  
`RESPONSE: {"status": "OK"}`

Cобрать локально образ приложения в докер.  
Запушить образ в dockerhub  

Написать манифесты для деплоя в k8s для этого сервиса.  

Манифесты должны описывать сущности Deployment, Service, Ingress.  
В Deployment могут быть указаны Liveness, Readiness пробы.  
Количество реплик должно быть не меньше 2. Image контейнера должен быть указан с Dockerhub.  

Хост в ингрессе должен быть arch.homework. В итоге после применения манифестов GET запрос на `http://arch.homework/health` должен отдавать `{“status”: “OK”}`.

На выходе предоставить  
0) ссылку на github c манифестами. Манифесты должны лежать в одной директории, так чтобы можно было их все применить одной командой kubectl apply -f .  
1) url, по которому можно будет получить ответ от сервиса (либо тест в postmanе).

Задание со звездой* (+5 баллов):

В Ingress-е должно быть правило, которое форвардит все запросы с /otusapp/{student name}/* на сервис с rewrite-ом пути. Где {student name} - это имя студента.  
Рекомендуем сдать до: 27.01.2021

## Solution/Решение

Текущее приложение и последующие написано на [Golang-e](https://golang.org/) в связи с этим файлы манифестов будут лежать не в корне проекта, а в папке deployments/..

- сервис [main.go](main.go)
- dockerfile [build/otus/hw1/Dockerfile](../../build/otus/hw1/Dockerfile)
- gitlab-ci собирает docker image `registry.gitlab.com/art.frela/data.platform:otushw1` (public!)
- манифесты в папке [deployments/otus/hw1/](../../deployments/otus/hw1/), после клонирования репозитория в корне проекта выполнить `kubectl apply -f deployments/otus/hw1/`
- проверить можно curl-ом: `curl -k http://localhost/health -L -H "Host: arch.homework"`
- так же прилагаю postman коллекцию [otus.hw1.postman_collection.json](../../tests/otus/hw1/otus.hw1.postman_collection.json)
- задание со звездочкой: `curl -k http://localhost/otusapp/artem/version -L -H "Host: arch.homework"`

> Lifehack for docker-desktop - install nginx-ingress-controller

`kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-0.32.0/deploy/static/provider/cloud/deploy.yaml`
