package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

const (
	version string = "0.0.9"
)

func healthz(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, `{"status": "OK"}`)
}

func httpVersion(w http.ResponseWriter, r *http.Request) {
	host, err := os.Hostname()
	if err != nil {
		host = err.Error()
	}
	fmt.Fprintf(w, `{"version": "%s", "request": "%s", "hostname": "%s"}`, version, r.URL.Path, host)
}

func main() {
	r := mux.NewRouter().StrictSlash(true)
	r.HandleFunc("/health/", healthz)
	r.HandleFunc("/version/", httpVersion)
	r.NotFoundHandler = http.HandlerFunc(httpVersion)

	port, ok := os.LookupEnv("PORT")
	if !ok {
		port = "8000"
	}

	loggedRouter := handlers.LoggingHandler(os.Stdout, r)
	log.Printf("Starting listening on :%v", port)
	log.Fatal(http.ListenAndServe(":"+port, loggedRouter))
}
