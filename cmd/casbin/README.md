# Abstract.access

Есть два списка (List), каждый из 4ех каких-то элементов Items с id=1,2...4 и атрибутом g=1 или g=2 по которому можно их группировать  
Со временем кол-во групп и элементов может изменяться.

Пример одного списка List1

```json
[
    {
        "item": {
            "id": 1,
            "g": 1
        }
    },
    {
        "item": {
            "id": 2,
            "g": 2
        }
    },
    {
        "item": {
            "id": 3,
            "g": 1
        }
    },
    {
        "item": {
            "id": 4,
            "g": 2
        }
    }
]
```

## Задача

Разграничить права доступа пользователей таким образом, что бы они могли корректно работать и при этом не могли получить несанкционированный доступ

### Операции, которые совершают пользователи

- [ ] Пользователь делает запрос списка списков и получает в ответ идентификаторы доступных ему списков
- [ ] Пользователь выбирает список (List) и делает запрос элементов и получает в ответ список доступных ему элементов
- [ ] Пользователь делает запрос на получение конкретного элемента из конкретного списка в ответ получает элемент если есть доступ и ошибку если доступа нет
- [ ] **C**RUD элемент - пользователь создает элемент в списке
- [ ] C**R**UD читает элемент (см п.п. выше)
- [ ] CR**U**D обновляет элемент в списке
- [ ] CRU**D** удаляет элемент из списка
- [ ] Читает данные измерений, привязанных к элементу или списку элементов из списка

### Пример списка пользователей, для которых надо назначить права доступа

- `user_main` - имеет доступ ко всем item-ам во всех списках, может делать все над элементами (админ)
- `user_l1g1` - имеет доступ ко всем элементам с атрибутом g=1 в списке 1 (List1) может создавать и удалять эдементы
- `user_l1_4` - имеет доступ к элементу с атрибутом id=4 в списке 1 (List1) может только читать сам элемент и его данные
- `user_l2g2` - имеет доступ ко всем элементам с атрибутом g=2 в списке 2 (List2) может только читать элементы и их данные
- `user_l2_1` - имеет доступ к элементу с атрибутом id=1 в списке 2 (List2) может только читать сам элемент и его данные

> По всему лучше всего в данном случае подходит модель ABAC
