package main

import (
	"log"

	"github.com/casbin/casbin/v2"
	mongodbadapter "github.com/casbin/mongodb-adapter/v3"
)

func main() {
	// Initialize a MongoDB adapter and use it in a Casbin enforcer:
	// The adapter will use the database named "casbin".
	// If it doesn't exist, the adapter will create it automatically.
	a, err := mongodbadapter.NewAdapter("127.0.0.1:27017") // Your MongoDB URL.
	if err != nil {
		panic(err)
	}
	e, err := casbin.NewEnforcer("../../asserts/sec/model.conf", a)
	if err != nil {
		panic(err)
	}

	// Load the policy from DB.
	err = e.LoadPolicy()
	if err != nil {
		panic(err)
	}

	// - `user_main` - имеет доступ ко всем item-ам во всех списках, может делать все над элементами (админ)
	ok, err := e.AddNamedPolicy("p", "user_main", "items", "/*", "true")
	log.Println("user_main: ok-err:", ok, err)

	// - `user_l1g1` - имеет доступ ко всем элементам с атрибутом g=1 в списке 1 (List1) может создавать и удалять эдементы
	ok, err = e.AddNamedPolicy("p", "user_l1g1", "items", "/*", "r.obj.ListID == 1 && r.obj.G == 1")
	log.Println("ok-err:", ok, err)

	// - `user_l1_4` - имеет доступ к элементу с атрибутом id=4 в списке 1 (List1) может только читать сам элемент и его данные
	ok, err = e.AddNamedPolicy("p", "user_l1_4", "items", "read", "r.obj.ListID == 1 && r.obj.ID == 4")
	log.Println("ok-err:", ok, err)

	// - `user_l2g2` - имеет доступ ко всем элементам с атрибутом g=2 в списке 2 (List2) может только читать элементы и их данные
	ok, err = e.AddNamedPolicy("p", "user_l2g2", "items", "read", "r.obj.ListID == 2 && r.obj.G == 2")
	log.Println("ok-err:", ok, err)

	// - `user_l2_1` - имеет доступ к элементу с атрибутом id=1 в списке 2 (List2) может только читать сам элемент и его данные
	ok, err = e.AddNamedPolicy("p", "user_l2_1", "items", "read", "r.obj.ListID == 2 && r.obj.ID == 1")
	log.Println("ok-err:", ok, err)

	type item struct {
		ID     int
		ListID int
		G      int
	}
	o1 := item{ID: 1, ListID: 1, G: 1}
	// Check the permission.
	ok, err = e.Enforce("user_main", o1, "read")
	log.Println("user_main-ok-err:", ok, err)
	ok, err = e.Enforce("user_main", o1, "write")
	log.Println("user_main-ok-err:", ok, err)

	// Check the permission.
	o2 := item{ID: 2, ListID: 1, G: 2}
	ok, err = e.Enforce("user_l1g1", o1, "read")
	log.Println("user_l1g1-o1-ok-err:", ok, err)
	//
	ok, err = e.Enforce("user_l1g1", o2, "read")
	log.Println("user_l1g1-o2-ok-err:", ok, err)

	perms, err := e.GetImplicitPermissionsForUser("user_l2_1")
	log.Println("user_l2_1_perms-err:", perms, err)
}
