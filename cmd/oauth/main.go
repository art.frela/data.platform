package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/gitlab"
)

const (
	// nolint: golint,stylecheck
	VK_APP_ID = "20d6066543f505b474c0a1e28fd8ef8af2b86b7a4ca7ecada64f080fae4abb38"
	// nolint: golint,stylecheck
	VK_APP_KEY = "09af1a4960159110ce12dc28ef61321834af3ca104c3a96a1c4140c9ca0beaf5"
	// куда идти с токеном за информацией
	// nolint: golint,stylecheck
	VK_API_URL = "https://gitlab.com/api/v4/user"
	// куда идти для получения токена
	// nolint: golint,stylecheck,lll
	VK_AUTH_URL = "https://gitlab.com/oauth/authorize?client_id=20d6066543f505b474c0a1e28fd8ef8af2b86b7a4ca7ecada64f080fae4abb38&redirect_uri=http://localhost:8081/user/login_oauth&response_type=code&scope=read_user"
)

type Response struct {
	FirstName string `json:"name"`
	Photo     string `json:"avatar_url"`
}

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		code := r.FormValue("code")

		if code == "" {
			_, err := w.Write([]byte(`<div><a href="` + VK_AUTH_URL + `">authorize</a></div>`))
			if err != nil {
				log.Fatalf("write response error, %s", err)
			}
			return
		}

		conf := oauth2.Config{
			ClientID:     VK_APP_ID,
			ClientSecret: VK_APP_KEY,
			RedirectURL:  "http://localhost:8081/user/login_oauth",
			Endpoint:     gitlab.Endpoint,
		}

		token, err := conf.Exchange(ctx, code)
		if err != nil {
			http.Error(w, "cannot exchange "+err.Error(), 500)
			return
		}
		log.Printf("token %#v\n", token)
		if token == nil {
			http.Error(w, "nil token", 500)
			return
		}
		email := ""      // token.Extra("email").(string)
		userIDraw := 1.0 // token.Extra("user_id").(float64)
		userID := int(userIDraw)

		_, err = w.Write([]byte(`
		<div> Oauth token:<br>
			` + fmt.Sprintf("%#v", token) + `
		</div>
		<div>Email: ` + email + `</div>
		<div>UserID: ` + strconv.Itoa(userID) + `</div>
		<br>
		`))
		if err != nil {
			log.Fatalf("write response error, %s", err)
		}

		client := conf.Client(ctx, token)
		resp, err := client.Get(VK_API_URL)
		if err != nil {
			http.Error(w, "request error "+err.Error(), 500)
			return
		}

		defer resp.Body.Close()

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			http.Error(w, "buf read err "+err.Error(), 500)
			return
		}
		log.Printf("body /user: %s\n", body)
		data := &Response{}
		err = json.Unmarshal(body, data)
		if err != nil {
			log.Fatalf("json.Unmarshal error, %s", err)
		}
		_, err = w.Write([]byte(`
		<div>
			<img src="` + data.Photo + `"/>
			` + data.FirstName + `
		</div>
		<br>
		<div> User info:<br>
			` + string(body) + `
		</div>
		`))
		if err != nil {
			log.Fatalf("write response error, %s", err)
		}
	})
	log.Println("start http :8081 tcp port")
	if err := http.ListenAndServe(":8081", nil); err != nil {
		log.Printf("http.ListenAndServe rror, %s", err)
	}
}
