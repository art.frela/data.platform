package main

import (
	"log"
	"os"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/gitlab"
)

func main() {
	m, err := migrate.New(
		os.Getenv("OTUSHW2_SOURCE"),
		os.Getenv("OTUSHW2_DB_URL"))
	if err != nil {
		log.Fatalf("for source %s migrate error %s", os.Getenv("OTUSHW2_SOURCE"), err)
	}
	// Migrate all the way up ...
	if err := m.Up(); err != nil && err != migrate.ErrNoChange {
		log.Fatalf("migrate.UP failed, %s", err)
	}
}
