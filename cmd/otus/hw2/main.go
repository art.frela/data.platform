package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"gitlab.com/art.frela/data.platform/internal/config"
	userhttp "gitlab.com/art.frela/data.platform/internal/users/http"
	"gitlab.com/art.frela/data.platform/pkg/logging"
	"gitlab.com/art.frela/data.platform/pkg/mergectx"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sethvargo/go-signalcontext"
)

const (
	appName          string = "otushw2"
	version          string = "0.0.6"
	logFormatConsole string = "console"
	envPrefix        string = "otushw2"
)

var (
	build   string = "_dev_build"
	githash string = "_dev_hash"
)

var (
	httpDuration = promauto.NewHistogramVec(prometheus.HistogramOpts{
		Name: "http_duration_milliseconds",
		Help: "Duration of HTTP requests.",
	}, []string{"path", "code", "method"})
)

func healthz(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, `{"status": "OK"}`)
}

func httpVersion(w http.ResponseWriter, r *http.Request) {
	host, err := os.Hostname()
	if err != nil {
		host = err.Error()
	}
	vars := mux.Vars(r)
	fmt.Fprintf(w, `{"version": "%s", "request": "%s", "hostname": "%s", "vars":%v}`, version, r.URL.Path, host, vars)
}

func main() {
	cfg := &config.Config{}
	v1, err := config.Read(appName, envPrefix, config.Defaults, cfg)
	if err != nil {
		if strings.Contains(err.Error(), "Not Found") {
			log.Println(err, "using default values")
		} else {
			log.Fatalf("cant read config, error: %s\n", err)
		}
	}
	// fmt.Printf("v1: %+v\ncfg: %+v", v1.AllSettings(), cfg)
	debug := false
	if strings.ToLower(v1.GetString("log.level")) == "debug" {
		debug = true
	}
	logger := logging.NewLogger(debug, logFormatConsole)
	logger = logger.With("version", version)
	logger = logger.With("build", build)
	logger = logger.With("githash", githash)

	ctx, cancel := signalcontext.OnInterrupt()
	defer cancel()
	// put logger to context
	ctx = logging.WithLogger(ctx, logger)
	dbURL := v1.GetString("db.url")
	dbProvider := v1.GetString("db.provider")
	uh, err := userhttp.New(ctx, dbProvider, dbURL, debug)
	if err != nil {
		logger.Fatalf("userhttp.New failed, %s", err)
	}

	r := mux.NewRouter().StrictSlash(true)

	r.HandleFunc("/health/", healthz)
	r.Path("/version/{id}").HandlerFunc(httpVersion)
	r.NotFoundHandler = http.HandlerFunc(httpVersion)
	// users
	uh.RegisterHandlers(r)
	// srv
	r.Use(middlewareWithCtx(ctx))
	r.Path("/metrics").Handler(promhttp.Handler())
	//
	addr := v1.GetString("httpd.host") + ":" + v1.GetString("httpd.port")
	srv := &http.Server{
		Addr: addr,
		// Good practice to set timeouts to avoid Slowloris attacks.
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      r, // Pass our instance of gorilla/mux in.
	}
	go func() {
		logger.Infof("Starting listening on %s", addr)
		if er := srv.ListenAndServe(); er != nil && er != http.ErrServerClosed {
			logger.Fatal(er)
		}
	}()
	<-ctx.Done()
	wait := time.Second * 15
	logger.Infof("got signal to shutdown, timeout %v", wait)
	ctxstop, cancelstop := context.WithTimeout(context.Background(), wait)
	defer cancelstop()
	err = srv.Shutdown(ctxstop)
	if err != nil {
		logger.Errorf("srv.Shutdown error, %s", err)
	}
}

// temp

func middlewareWithCtx(ctx context.Context) func(http.Handler) http.Handler {
	//
	mw := func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			// Do stuff here
			newctx := mergectx.MergeContexts(ctx, r.Context())
			r = r.WithContext(newctx)
			// Call the next handler, which can be another middleware in the chain, or the final handler.
			log := logging.FromContext(ctx)
			start := time.Now()
			route := mux.CurrentRoute(r)
			path, err := route.GetPathTemplate()
			if err != nil {
				path = "/undefined"
			}
			crw := newCodeResponseWriter(w)
			next.ServeHTTP(crw, r)

			log.With(
				"method", r.Method,
				"proto", r.Proto,
				"remote", r.RemoteAddr,
				"url", path,
				"code", crw.statusCode,
				"duration", time.Since(start).String(),
			).Info("request")
			httpDuration.WithLabelValues(path, strconv.Itoa(crw.statusCode), r.Method).
				Observe(float64(time.Since(start).Milliseconds()))
		})
	}
	return mw
}

// metrics

type codeResponseWriter struct {
	http.ResponseWriter
	statusCode int
}

func newCodeResponseWriter(w http.ResponseWriter) *codeResponseWriter {
	return &codeResponseWriter{w, http.StatusOK}
}

func (crw *codeResponseWriter) WriteHeader(code int) {
	crw.statusCode = code
	crw.ResponseWriter.WriteHeader(code)
}
