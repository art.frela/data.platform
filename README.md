# data.platform

>Project for study purpose - MSA with complex access system

- [data.platform](#dataplatform)
  - [Goals](#goals)
  - [Subject (MSA system model)](#subject-msa-system-model)
  - [CodeBase organizing](#codebase-organizing)
  - [Details](#details)
    - [Measurements collecting](#measurements-collecting)
    - [Data processing, data access and visualizing](#data-processing-data-access-and-visualizing)

## Goals

- [ ] Studying how to implement RBAC pattern in complex distributed systems
- [ ] Studying how and where stores access rights in that system
- [ ] Studying how best to organize this data and why this is the case
- [ ] Use two or more types of datastorages (ex.: relation database, TSDB, NoSQL (documents)..., caches (Redis), Queues)
- [ ] Implements base working model (some MVP)
- [ ] Applying openTelemetry (opentracing/opencensus) for tracing, logs, metrics
- [ ] Applying best practics for complex systems: ci/cd, observability, scalability.. etc... -> got telegram messagre from prometheus alert manager if some happen
- [ ] Applying vault for secret storage in k8s and using that by apps
- [ ] TRY BDD with [cucumber/godog](https://github.com/cucumber/godog)

![goals](asserts/img/data.platform.main.png)

## Subject (MSA system model)

System measurements processing and visualizing for trade objects.  
The working system is a www-cabinet for business users, where they can see graphics and tables (lik reports) of measurement results specified trade-objects.  
Работающая система предоставляет бизнес пользователям доступ в вэб кабинет, в котором можно просмотреть результаты измерений, дополнительно расчитанные метрики по обработанным измерениям торговых объектов.

```plantuml
node "Measurements collecting" {
  [Sensor] - [Controller]
  [DataCollector]
}

node "Device management" {
  [DeviceManager] - [Controller]
}

node "Data processing, data access and visualizing" {
  [DataStorage]
  [WWW]
  [ACaaS]
  [AnomalyDetector]
}

[Controller] --> [DataCollector] : measurements
[DataCollector] --> [DataStorage] : measurements
[AnomalyDetector] <-- [DataStorage] : metrics
[WWW] <-- [DataStorage] : metrics
[Users] --> [WWW]
[WWW] --> [ACaaS] : access request
```

Block **Measurements collecting** will be mocked by data-collector simulator  
Block **Device management** will be missed  
Block **Data processing, data access and visualizing** will be decomposed below  

## CodeBase organizing

Monorepo with golang-project-layout style

## Details

### Measurements collecting

> first implementation will be simple periodic inserts raw measurements to the datastorage

### Data processing, data access and visualizing

>