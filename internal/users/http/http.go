// Package http contains http handle functions for user CRUD operations
package http

import (
	"context"
	"encoding/json"
	"net/http"
	"strconv"

	"gitlab.com/art.frela/data.platform/internal/users/model"
	"gitlab.com/art.frela/data.platform/internal/users/repo"
	"gitlab.com/art.frela/data.platform/pkg/logging"

	"github.com/gorilla/mux"
	"github.com/pkg/errors"
)

//go:generate easyjson -all http.go

const (
	// PAGESIZE number of document per page by default
	PAGESIZE int = 20
)

// UserHandler http.HandleFunc implementations
type UserHandler struct {
	repo repo.StorageInterface
}

// New builder for UserHandler
func New(ctx context.Context, provider, url string, debug bool) (*UserHandler, error) {
	repo, err := repo.New(ctx, provider, url, debug)
	if err != nil {
		return nil, errors.WithMessage(err, "repo.New failed")
	}
	return &UserHandler{repo: repo}, nil
}

// UsersResponse users and metadata
type UsersResponse struct {
	Data model.Users `json:"data,omitempty"`
	Metadata
}

// List returns list of the users, with pagination
func (uh *UserHandler) List(w http.ResponseWriter, r *http.Request) {
	log := logging.FromContext(r.Context())
	offset, limit := uh.getPageParams(r)
	users, count, err := uh.repo.FindAll(r.Context(), offset, limit)
	switch err {
	case nil:
		// all is ok
	case repo.ErrNotFound:
		http.Error(w, "No users", http.StatusBadRequest)
		return
	default:
		http.Error(w, "Db err", http.StatusInternalServerError)
		return
	}
	response := UsersResponse{
		Data: users,
		Metadata: Metadata{
			ResultSet: ResultSet{
				Count:  int64(len(users)),
				Offset: offset,
				Limit:  limit,
				Total:  count,
			},
		},
	}
	bts, err := response.MarshalJSON()
	if err != nil {
		http.Error(w, "marshal err", http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	_, err = w.Write(bts)
	if err != nil {
		log.Errorf("write response error", err)
	}
}

// single returns single user by specified id
func (uh *UserHandler) Single(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	log := logging.FromContext(r.Context())
	uid, ok := vars["id"]
	if !ok {
		log.Error("empty id not allowed")
		http.Error(w, "empty id not allowed", http.StatusBadRequest)
		return
	}
	ud, err := uh.repo.FindByID(r.Context(), uid)
	if err != nil {
		log.Errorf("findByID from the repo error, %s", err)
		http.Error(w, "db err", http.StatusInternalServerError)
		return
	}
	if err == repo.ErrNotFound {
		http.Error(w, "user not found", http.StatusNotFound)
		return
	}

	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(ud)
	if err != nil {
		log.Errorf("encode error, %s", err)
	}
}

type NewUserRequest struct {
	Login    string `json:"login,omitempty"`
	EMail    string `json:"email,omitempty"`
	Password string `json:"password,omitempty"`
}

func (ur NewUserRequest) User() model.User {
	return model.User{
		Login: ur.Login,
		EMail: ur.EMail,
	}
}

// Create creates new user
func (uh *UserHandler) Create(w http.ResponseWriter, r *http.Request) {
	log := logging.FromContext(r.Context())
	var newUser NewUserRequest
	err := json.NewDecoder(r.Body).Decode(&newUser)
	if err != nil {
		log.Errorf("decode request body error, %s", err)
		http.Error(w, "request body err", http.StatusBadRequest)
		return
	}
	u, err := uh.repo.Store(r.Context(), newUser.User(), newUser.Password)
	if err != nil {
		log.Errorf("store to repo error", err)
		http.Error(w, "db err", http.StatusInternalServerError)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	err = json.NewEncoder(w).Encode(&u)
	if err != nil {
		log.Errorf("encode error, %s", err)
	}
}

type UpdUserRequest struct {
	Login    string `json:"login,omitempty"`
	EMail    string `json:"email,omitempty"`
	Password string `json:"password,omitempty"`
}

func (ur UpdUserRequest) User(id string) model.User {

	u := model.User{
		ID:    id,
		Login: ur.Login,
		EMail: ur.EMail,
	}
	if ur.Password != "" {
		u.Password = u.HashPass(model.MakeSalt(model.SaltLen), ur.Password)
	}
	return u
}

// Update updates exists user
func (uh *UserHandler) Update(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	log := logging.FromContext(r.Context())
	var user UpdUserRequest
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		log.Errorf("decode request body error, %s", err)
		http.Error(w, "request body err", http.StatusBadRequest)
		return
	}
	uid, ok := vars["id"]
	if !ok {
		log.Error("empty id not allowed")
		http.Error(w, "empty id not allowed", http.StatusBadRequest)
		return
	}
	_, err = uh.repo.Update(r.Context(), user.User(uid))
	if err != nil {
		log.Errorf("update in the repo error, %s", err)
		http.Error(w, "db err", http.StatusInternalServerError)
		return
	}
	if user.Password != "" {
		_, err = uh.repo.UpdatePassword(r.Context(), user.User(uid))
		if err != nil {
			log.Errorf("update in the repo error", err)
			http.Error(w, "db err", http.StatusInternalServerError)
			return
		}
	}
	w.WriteHeader(http.StatusOK)

	err = json.NewEncoder(w).Encode(map[string]interface{}{"message": "user updated"})
	if err != nil {
		log.Errorf("encode error, %s", err)
	}
}

// Delete deletes exists user
func (uh *UserHandler) Delete(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	log := logging.FromContext(r.Context())
	uid, ok := vars["id"]
	if !ok {
		log.Error("empty id not allowed")
		http.Error(w, "empty id not allowed", http.StatusBadRequest)
		return
	}
	ud, err := uh.repo.Delete(r.Context(), model.User{ID: uid})
	if err != nil {
		log.Errorf("delete from the repo error, %s", err)
		http.Error(w, "db err", http.StatusInternalServerError)
		return
	}
	if ud != 1 {
		w.WriteHeader(http.StatusNotFound)
		err = json.NewEncoder(w).Encode(map[string]interface{}{"message": "user not found"})
		if err != nil {
			log.Errorf("encode error, %s", err)
		}
		return
	}
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(map[string]interface{}{"message": "user deleted"})
	if err != nil {
		log.Errorf("encode error, %s", err)
	}
}

// Register handlers and it's sub router
func (uh *UserHandler) RegisterHandlers(r *mux.Router) {
	// r.StrictSlash(true)
	u := r.PathPrefix("/users").Subrouter()
	u.HandleFunc("", uh.List).Methods("GET")
	u.HandleFunc("", uh.Create).Methods("POST")
	u.HandleFunc("/{id}", uh.Single).Methods("GET")
	u.HandleFunc("/{id}", uh.Update).Methods("PUT")
	u.HandleFunc("/{id}", uh.Delete).Methods("DELETE")
}

// helpers

// getPageParams parse http request parameters and returns offset and limit
func (uh *UserHandler) getPageParams(r *http.Request) (offset, limit int64) {
	slimit := r.URL.Query().Get("limit")
	if slimit == "" {
		slimit = "0"
	}
	ilimit, err := strconv.Atoi(slimit)
	if err != nil {
		ilimit = PAGESIZE
	}
	if ilimit <= 0 {
		ilimit = PAGESIZE
	}

	soffset := r.URL.Query().Get("offset")
	if soffset == "" {
		soffset = "0"
	}
	ioffset, err := strconv.Atoi(soffset)
	if err != nil {
		ioffset = 0
	}
	if ioffset < 0 {
		ioffset = 0
	}
	limit, offset = int64(ilimit), int64(ioffset)
	return
}

// Metadata - metadata, page, limit, offset... etc...
type Metadata struct {
	ResultSet ResultSet `json:"result_set"`
}

// ResultSet - values total, limit....
type ResultSet struct {
	Count  int64 `json:"count"`
	Offset int64 `json:"offset"`
	Limit  int64 `json:"limit"`
	Total  int64 `json:"total"`
}
