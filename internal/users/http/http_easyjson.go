// Code generated by easyjson for marshaling/unmarshaling. DO NOT EDIT.

package http

import (
	json "encoding/json"
	easyjson "github.com/mailru/easyjson"
	jlexer "github.com/mailru/easyjson/jlexer"
	jwriter "github.com/mailru/easyjson/jwriter"
	model "gitlab.com/art.frela/data.platform/internal/users/model"
	time "time"
)

// suppress unused package warning
var (
	_ *json.RawMessage
	_ *jlexer.Lexer
	_ *jwriter.Writer
	_ easyjson.Marshaler
)

func easyjson6252c418DecodeGitlabComArtFrelaDataPlatformInternalUsersHttp(in *jlexer.Lexer, out *UsersResponse) {
	isTopLevel := in.IsStart()
	if in.IsNull() {
		if isTopLevel {
			in.Consumed()
		}
		in.Skip()
		return
	}
	in.Delim('{')
	for !in.IsDelim('}') {
		key := in.UnsafeFieldName(false)
		in.WantColon()
		if in.IsNull() {
			in.Skip()
			in.WantComma()
			continue
		}
		switch key {
		case "data":
			if in.IsNull() {
				in.Skip()
				out.Data = nil
			} else {
				in.Delim('[')
				if out.Data == nil {
					if !in.IsDelim(']') {
						out.Data = make(model.Users, 0, 0)
					} else {
						out.Data = model.Users{}
					}
				} else {
					out.Data = (out.Data)[:0]
				}
				for !in.IsDelim(']') {
					var v1 model.User
					easyjson6252c418DecodeGitlabComArtFrelaDataPlatformInternalUsersModel(in, &v1)
					out.Data = append(out.Data, v1)
					in.WantComma()
				}
				in.Delim(']')
			}
		case "result_set":
			(out.ResultSet).UnmarshalEasyJSON(in)
		default:
			in.SkipRecursive()
		}
		in.WantComma()
	}
	in.Delim('}')
	if isTopLevel {
		in.Consumed()
	}
}
func easyjson6252c418EncodeGitlabComArtFrelaDataPlatformInternalUsersHttp(out *jwriter.Writer, in UsersResponse) {
	out.RawByte('{')
	first := true
	_ = first
	if len(in.Data) != 0 {
		const prefix string = ",\"data\":"
		first = false
		out.RawString(prefix[1:])
		{
			out.RawByte('[')
			for v2, v3 := range in.Data {
				if v2 > 0 {
					out.RawByte(',')
				}
				easyjson6252c418EncodeGitlabComArtFrelaDataPlatformInternalUsersModel(out, v3)
			}
			out.RawByte(']')
		}
	}
	{
		const prefix string = ",\"result_set\":"
		if first {
			first = false
			out.RawString(prefix[1:])
		} else {
			out.RawString(prefix)
		}
		(in.ResultSet).MarshalEasyJSON(out)
	}
	out.RawByte('}')
}

// MarshalJSON supports json.Marshaler interface
func (v UsersResponse) MarshalJSON() ([]byte, error) {
	w := jwriter.Writer{}
	easyjson6252c418EncodeGitlabComArtFrelaDataPlatformInternalUsersHttp(&w, v)
	return w.Buffer.BuildBytes(), w.Error
}

// MarshalEasyJSON supports easyjson.Marshaler interface
func (v UsersResponse) MarshalEasyJSON(w *jwriter.Writer) {
	easyjson6252c418EncodeGitlabComArtFrelaDataPlatformInternalUsersHttp(w, v)
}

// UnmarshalJSON supports json.Unmarshaler interface
func (v *UsersResponse) UnmarshalJSON(data []byte) error {
	r := jlexer.Lexer{Data: data}
	easyjson6252c418DecodeGitlabComArtFrelaDataPlatformInternalUsersHttp(&r, v)
	return r.Error()
}

// UnmarshalEasyJSON supports easyjson.Unmarshaler interface
func (v *UsersResponse) UnmarshalEasyJSON(l *jlexer.Lexer) {
	easyjson6252c418DecodeGitlabComArtFrelaDataPlatformInternalUsersHttp(l, v)
}
func easyjson6252c418DecodeGitlabComArtFrelaDataPlatformInternalUsersModel(in *jlexer.Lexer, out *model.User) {
	isTopLevel := in.IsStart()
	if in.IsNull() {
		if isTopLevel {
			in.Consumed()
		}
		in.Skip()
		return
	}
	in.Delim('{')
	for !in.IsDelim('}') {
		key := in.UnsafeFieldName(false)
		in.WantColon()
		if in.IsNull() {
			in.Skip()
			in.WantComma()
			continue
		}
		switch key {
		case "id":
			out.ID = string(in.String())
		case "login":
			out.Login = string(in.String())
		case "email":
			out.EMail = string(in.String())
		case "creator":
			out.Creator = string(in.String())
		case "created_at":
			if data := in.Raw(); in.Ok() {
				in.AddError((out.CreatedAt).UnmarshalJSON(data))
			}
		case "modifier":
			if in.IsNull() {
				in.Skip()
				out.Modifier = nil
			} else {
				if out.Modifier == nil {
					out.Modifier = new(string)
				}
				*out.Modifier = string(in.String())
			}
		case "modified_at":
			if in.IsNull() {
				in.Skip()
				out.ModifiedAt = nil
			} else {
				if out.ModifiedAt == nil {
					out.ModifiedAt = new(time.Time)
				}
				if data := in.Raw(); in.Ok() {
					in.AddError((*out.ModifiedAt).UnmarshalJSON(data))
				}
			}
		default:
			in.SkipRecursive()
		}
		in.WantComma()
	}
	in.Delim('}')
	if isTopLevel {
		in.Consumed()
	}
}
func easyjson6252c418EncodeGitlabComArtFrelaDataPlatformInternalUsersModel(out *jwriter.Writer, in model.User) {
	out.RawByte('{')
	first := true
	_ = first
	if in.ID != "" {
		const prefix string = ",\"id\":"
		first = false
		out.RawString(prefix[1:])
		out.String(string(in.ID))
	}
	if in.Login != "" {
		const prefix string = ",\"login\":"
		if first {
			first = false
			out.RawString(prefix[1:])
		} else {
			out.RawString(prefix)
		}
		out.String(string(in.Login))
	}
	if in.EMail != "" {
		const prefix string = ",\"email\":"
		if first {
			first = false
			out.RawString(prefix[1:])
		} else {
			out.RawString(prefix)
		}
		out.String(string(in.EMail))
	}
	if in.Creator != "" {
		const prefix string = ",\"creator\":"
		if first {
			first = false
			out.RawString(prefix[1:])
		} else {
			out.RawString(prefix)
		}
		out.String(string(in.Creator))
	}
	if true {
		const prefix string = ",\"created_at\":"
		if first {
			first = false
			out.RawString(prefix[1:])
		} else {
			out.RawString(prefix)
		}
		out.Raw((in.CreatedAt).MarshalJSON())
	}
	if in.Modifier != nil {
		const prefix string = ",\"modifier\":"
		if first {
			first = false
			out.RawString(prefix[1:])
		} else {
			out.RawString(prefix)
		}
		out.String(string(*in.Modifier))
	}
	if in.ModifiedAt != nil {
		const prefix string = ",\"modified_at\":"
		if first {
			first = false
			out.RawString(prefix[1:])
		} else {
			out.RawString(prefix)
		}
		out.Raw((*in.ModifiedAt).MarshalJSON())
	}
	out.RawByte('}')
}
func easyjson6252c418DecodeGitlabComArtFrelaDataPlatformInternalUsersHttp1(in *jlexer.Lexer, out *UserHandler) {
	isTopLevel := in.IsStart()
	if in.IsNull() {
		if isTopLevel {
			in.Consumed()
		}
		in.Skip()
		return
	}
	in.Delim('{')
	for !in.IsDelim('}') {
		key := in.UnsafeFieldName(false)
		in.WantColon()
		if in.IsNull() {
			in.Skip()
			in.WantComma()
			continue
		}
		switch key {
		default:
			in.SkipRecursive()
		}
		in.WantComma()
	}
	in.Delim('}')
	if isTopLevel {
		in.Consumed()
	}
}
func easyjson6252c418EncodeGitlabComArtFrelaDataPlatformInternalUsersHttp1(out *jwriter.Writer, in UserHandler) {
	out.RawByte('{')
	first := true
	_ = first
	out.RawByte('}')
}

// MarshalJSON supports json.Marshaler interface
func (v UserHandler) MarshalJSON() ([]byte, error) {
	w := jwriter.Writer{}
	easyjson6252c418EncodeGitlabComArtFrelaDataPlatformInternalUsersHttp1(&w, v)
	return w.Buffer.BuildBytes(), w.Error
}

// MarshalEasyJSON supports easyjson.Marshaler interface
func (v UserHandler) MarshalEasyJSON(w *jwriter.Writer) {
	easyjson6252c418EncodeGitlabComArtFrelaDataPlatformInternalUsersHttp1(w, v)
}

// UnmarshalJSON supports json.Unmarshaler interface
func (v *UserHandler) UnmarshalJSON(data []byte) error {
	r := jlexer.Lexer{Data: data}
	easyjson6252c418DecodeGitlabComArtFrelaDataPlatformInternalUsersHttp1(&r, v)
	return r.Error()
}

// UnmarshalEasyJSON supports easyjson.Unmarshaler interface
func (v *UserHandler) UnmarshalEasyJSON(l *jlexer.Lexer) {
	easyjson6252c418DecodeGitlabComArtFrelaDataPlatformInternalUsersHttp1(l, v)
}
func easyjson6252c418DecodeGitlabComArtFrelaDataPlatformInternalUsersHttp2(in *jlexer.Lexer, out *ResultSet) {
	isTopLevel := in.IsStart()
	if in.IsNull() {
		if isTopLevel {
			in.Consumed()
		}
		in.Skip()
		return
	}
	in.Delim('{')
	for !in.IsDelim('}') {
		key := in.UnsafeFieldName(false)
		in.WantColon()
		if in.IsNull() {
			in.Skip()
			in.WantComma()
			continue
		}
		switch key {
		case "count":
			out.Count = int64(in.Int64())
		case "offset":
			out.Offset = int64(in.Int64())
		case "limit":
			out.Limit = int64(in.Int64())
		case "total":
			out.Total = int64(in.Int64())
		default:
			in.SkipRecursive()
		}
		in.WantComma()
	}
	in.Delim('}')
	if isTopLevel {
		in.Consumed()
	}
}
func easyjson6252c418EncodeGitlabComArtFrelaDataPlatformInternalUsersHttp2(out *jwriter.Writer, in ResultSet) {
	out.RawByte('{')
	first := true
	_ = first
	{
		const prefix string = ",\"count\":"
		out.RawString(prefix[1:])
		out.Int64(int64(in.Count))
	}
	{
		const prefix string = ",\"offset\":"
		out.RawString(prefix)
		out.Int64(int64(in.Offset))
	}
	{
		const prefix string = ",\"limit\":"
		out.RawString(prefix)
		out.Int64(int64(in.Limit))
	}
	{
		const prefix string = ",\"total\":"
		out.RawString(prefix)
		out.Int64(int64(in.Total))
	}
	out.RawByte('}')
}

// MarshalJSON supports json.Marshaler interface
func (v ResultSet) MarshalJSON() ([]byte, error) {
	w := jwriter.Writer{}
	easyjson6252c418EncodeGitlabComArtFrelaDataPlatformInternalUsersHttp2(&w, v)
	return w.Buffer.BuildBytes(), w.Error
}

// MarshalEasyJSON supports easyjson.Marshaler interface
func (v ResultSet) MarshalEasyJSON(w *jwriter.Writer) {
	easyjson6252c418EncodeGitlabComArtFrelaDataPlatformInternalUsersHttp2(w, v)
}

// UnmarshalJSON supports json.Unmarshaler interface
func (v *ResultSet) UnmarshalJSON(data []byte) error {
	r := jlexer.Lexer{Data: data}
	easyjson6252c418DecodeGitlabComArtFrelaDataPlatformInternalUsersHttp2(&r, v)
	return r.Error()
}

// UnmarshalEasyJSON supports easyjson.Unmarshaler interface
func (v *ResultSet) UnmarshalEasyJSON(l *jlexer.Lexer) {
	easyjson6252c418DecodeGitlabComArtFrelaDataPlatformInternalUsersHttp2(l, v)
}
func easyjson6252c418DecodeGitlabComArtFrelaDataPlatformInternalUsersHttp3(in *jlexer.Lexer, out *NewUserRequest) {
	isTopLevel := in.IsStart()
	if in.IsNull() {
		if isTopLevel {
			in.Consumed()
		}
		in.Skip()
		return
	}
	in.Delim('{')
	for !in.IsDelim('}') {
		key := in.UnsafeFieldName(false)
		in.WantColon()
		if in.IsNull() {
			in.Skip()
			in.WantComma()
			continue
		}
		switch key {
		case "login":
			out.Login = string(in.String())
		case "email":
			out.EMail = string(in.String())
		default:
			in.SkipRecursive()
		}
		in.WantComma()
	}
	in.Delim('}')
	if isTopLevel {
		in.Consumed()
	}
}
func easyjson6252c418EncodeGitlabComArtFrelaDataPlatformInternalUsersHttp3(out *jwriter.Writer, in NewUserRequest) {
	out.RawByte('{')
	first := true
	_ = first
	if in.Login != "" {
		const prefix string = ",\"login\":"
		first = false
		out.RawString(prefix[1:])
		out.String(string(in.Login))
	}
	if in.EMail != "" {
		const prefix string = ",\"email\":"
		if first {
			first = false
			out.RawString(prefix[1:])
		} else {
			out.RawString(prefix)
		}
		out.String(string(in.EMail))
	}
	out.RawByte('}')
}

// MarshalJSON supports json.Marshaler interface
func (v NewUserRequest) MarshalJSON() ([]byte, error) {
	w := jwriter.Writer{}
	easyjson6252c418EncodeGitlabComArtFrelaDataPlatformInternalUsersHttp3(&w, v)
	return w.Buffer.BuildBytes(), w.Error
}

// MarshalEasyJSON supports easyjson.Marshaler interface
func (v NewUserRequest) MarshalEasyJSON(w *jwriter.Writer) {
	easyjson6252c418EncodeGitlabComArtFrelaDataPlatformInternalUsersHttp3(w, v)
}

// UnmarshalJSON supports json.Unmarshaler interface
func (v *NewUserRequest) UnmarshalJSON(data []byte) error {
	r := jlexer.Lexer{Data: data}
	easyjson6252c418DecodeGitlabComArtFrelaDataPlatformInternalUsersHttp3(&r, v)
	return r.Error()
}

// UnmarshalEasyJSON supports easyjson.Unmarshaler interface
func (v *NewUserRequest) UnmarshalEasyJSON(l *jlexer.Lexer) {
	easyjson6252c418DecodeGitlabComArtFrelaDataPlatformInternalUsersHttp3(l, v)
}
func easyjson6252c418DecodeGitlabComArtFrelaDataPlatformInternalUsersHttp4(in *jlexer.Lexer, out *Metadata) {
	isTopLevel := in.IsStart()
	if in.IsNull() {
		if isTopLevel {
			in.Consumed()
		}
		in.Skip()
		return
	}
	in.Delim('{')
	for !in.IsDelim('}') {
		key := in.UnsafeFieldName(false)
		in.WantColon()
		if in.IsNull() {
			in.Skip()
			in.WantComma()
			continue
		}
		switch key {
		case "result_set":
			(out.ResultSet).UnmarshalEasyJSON(in)
		default:
			in.SkipRecursive()
		}
		in.WantComma()
	}
	in.Delim('}')
	if isTopLevel {
		in.Consumed()
	}
}
func easyjson6252c418EncodeGitlabComArtFrelaDataPlatformInternalUsersHttp4(out *jwriter.Writer, in Metadata) {
	out.RawByte('{')
	first := true
	_ = first
	{
		const prefix string = ",\"result_set\":"
		out.RawString(prefix[1:])
		(in.ResultSet).MarshalEasyJSON(out)
	}
	out.RawByte('}')
}

// MarshalJSON supports json.Marshaler interface
func (v Metadata) MarshalJSON() ([]byte, error) {
	w := jwriter.Writer{}
	easyjson6252c418EncodeGitlabComArtFrelaDataPlatformInternalUsersHttp4(&w, v)
	return w.Buffer.BuildBytes(), w.Error
}

// MarshalEasyJSON supports easyjson.Marshaler interface
func (v Metadata) MarshalEasyJSON(w *jwriter.Writer) {
	easyjson6252c418EncodeGitlabComArtFrelaDataPlatformInternalUsersHttp4(w, v)
}

// UnmarshalJSON supports json.Unmarshaler interface
func (v *Metadata) UnmarshalJSON(data []byte) error {
	r := jlexer.Lexer{Data: data}
	easyjson6252c418DecodeGitlabComArtFrelaDataPlatformInternalUsersHttp4(&r, v)
	return r.Error()
}

// UnmarshalEasyJSON supports easyjson.Unmarshaler interface
func (v *Metadata) UnmarshalEasyJSON(l *jlexer.Lexer) {
	easyjson6252c418DecodeGitlabComArtFrelaDataPlatformInternalUsersHttp4(l, v)
}
