// +build integration

package repo_test

import (
	"context"
	"fmt"
	"math/rand"
	"reflect"
	"strconv"
	"strings"
	"testing"
	"time"

	"gitlab.com/art.frela/data.platform/internal/users/model"
	"gitlab.com/art.frela/data.platform/internal/users/repo"
)

const (
	cs    string = "postgres://postgres:secretpassword@localhost:5432/users?sslmode=disable"
	debug bool   = false
)

func TestPG_Store(t *testing.T) {
	ctx, cancel := context.WithCancel(context.TODO())
	defer cancel()
	u := mockNewUser()
	tests := []struct {
		name          string
		u             model.User
		plainPassword string
		want          model.User
		wantErr       bool
	}{
		{"success_vasya", u, "love", u, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			pg, err := repo.NewPG(ctx, cs, debug)
			if err != nil {
				t.Errorf("unexpected connect to db error, %s", err)
				return
			}
			got, err := pg.Store(ctx, tt.u, tt.plainPassword)
			if (err != nil) != tt.wantErr {
				t.Errorf("PG.Store() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			// for correct comaprison
			tt.want.ID = got.ID
			if !reflect.DeepEqual(*got, tt.want) {
				t.Errorf("PG.Store() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPG_FindByID(t *testing.T) {
	ctx, cancel := context.WithCancel(context.TODO())
	defer cancel()
	pg, err := repo.NewPG(ctx, cs, debug)
	if err != nil {
		t.Errorf("unexpected connect to db error, %s", err)
		return
	}
	defer pg.Close()

	type test struct {
		name    string
		id      string
		want    model.User
		wantErr bool
	}
	tests := []test{}
	for i := 0; i < 5; i++ {
		iu, err := pg.Store(ctx, mockNewUser(), "pass")
		if err != nil {
			t.Errorf("prepare users error, %s", err)
			return
		}
		tests = append(tests, test{
			iu.Login, iu.ID, *iu, false,
		})
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := pg.FindByID(ctx, tt.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("PG.FindByID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			// for correct compare
			tt.want.Creator = got.Creator
			tt.want.CreatedAt = got.CreatedAt
			tt.want.Password = got.Password
			if !reflect.DeepEqual(*got, tt.want) {
				t.Errorf("PG.FindByID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPG_FinfByLogin(t *testing.T) {
	ctx, cancel := context.WithCancel(context.TODO())
	defer cancel()
	pg, err := repo.NewPG(ctx, cs, debug)
	if err != nil {
		t.Errorf("unexpected connect to db error, %s", err)
		return
	}
	defer pg.Close()

	type test struct {
		name    string
		login   string
		want    model.User
		wantErr bool
	}
	tests := []test{}
	for i := 0; i < 5; i++ {
		iu, err := pg.Store(ctx, mockNewUser(), "pass")
		if err != nil {
			t.Errorf("prepare users error, %s", err)
			return
		}
		tests = append(tests, test{
			iu.Login, iu.Login, *iu, false,
		})
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := pg.FinfByLogin(ctx, tt.login)
			if (err != nil) != tt.wantErr {
				t.Errorf("PG.FinfByLogin() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			// for correct compare
			tt.want.Creator = got.Creator
			tt.want.CreatedAt = got.CreatedAt
			tt.want.Password = got.Password
			if !reflect.DeepEqual(*got, tt.want) {
				t.Errorf("PG.FinfByLogin() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPG_FinfByEMail(t *testing.T) {
	ctx, cancel := context.WithCancel(context.TODO())
	defer cancel()
	pg, err := repo.NewPG(ctx, cs, debug)
	if err != nil {
		t.Errorf("unexpected connect to db error, %s", err)
		return
	}
	defer pg.Close()

	type test struct {
		name    string
		email   string
		want    model.User
		wantErr bool
	}
	tests := []test{}
	for i := 0; i < 5; i++ {
		iu, err := pg.Store(ctx, mockNewUser(), "pass")
		if err != nil {
			t.Errorf("prepare users error, %s", err)
			return
		}
		tests = append(tests, test{
			iu.Login, iu.EMail, *iu, false,
		})
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := pg.FinfByEMail(ctx, tt.email)
			if (err != nil) != tt.wantErr {
				t.Errorf("PG.FinfByEMail() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			// for correct compare
			tt.want.Creator = got.Creator
			tt.want.CreatedAt = got.CreatedAt
			tt.want.Password = got.Password
			if !reflect.DeepEqual(*got, tt.want) {
				t.Errorf("PG.FinfByEMail() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPG_FindAll(t *testing.T) {
	ctx, cancel := context.WithCancel(context.TODO())
	defer cancel()
	pg, err := repo.NewPG(ctx, cs, debug)
	if err != nil {
		t.Errorf("unexpected connect to db error, %s", err)
		return
	}
	defer pg.Close()
	// prepara data
	users := make(model.Users, 20)
	for i := 0; i < 20; i++ {
		iu, err := pg.Store(ctx, mockNewUser(), "pass")
		if err != nil {
			t.Errorf("prepare users error, %s", err)
			return
		}
		users[i] = *iu
	}
	tests := []struct {
		name    string
		offset  int64
		limit   int64
		want    int
		want1   int64
		wantErr bool
	}{
		{"correct_0_5", 0, 5, 5, 20, false},
		{"correct_5_5", 0, 5, 5, 20, false},
		{"correct_0_0", 0, 0, 0, 20, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1, err := pg.FindAll(ctx, tt.offset, tt.limit)
			if (err != nil) != tt.wantErr {
				t.Errorf("PG.FindAll() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if len(got) != tt.want {
				t.Errorf("PG.FindAll() got.len = %d, want %d", len(got), tt.want)
			}
			if got1 < tt.want1 {
				t.Errorf("PG.FindAll() got1 = %v, want >= %v", got1, tt.want1)
			}
		})
	}
}

func TestPG_Update(t *testing.T) {
	ctx, cancel := context.WithCancel(context.TODO())
	defer cancel()
	pg, err := repo.NewPG(ctx, cs, debug)
	if err != nil {
		t.Errorf("unexpected connect to db error, %s", err)
		return
	}
	defer pg.Close()
	// prepara data
	iu, err := pg.Store(ctx, mockNewUser(), "pass")
	if err != nil {
		t.Errorf("prepare users error, %s", err)
		return
	}
	u1 := *iu
	u1.Login = mockNewUser().Login
	u2 := *iu
	u2.EMail = mockNewUser().EMail
	tests := []struct {
		name    string
		u       model.User
		want    int64
		wantErr bool
	}{
		{"u1_login", u1, 1, false},
		{"u1_email", u2, 1, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := pg.Update(ctx, tt.u)
			if (err != nil) != tt.wantErr {
				t.Errorf("PG.Update() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("PG.Update() = %v, want %v", got, tt.want)
			}
			gotcheck, err := pg.FindByID(ctx, tt.u.ID)
			if err != nil {
				t.Errorf("PG.FindByID() error = %v", err)
				return
			}
			if tt.u.Login != gotcheck.Login || tt.u.EMail != gotcheck.EMail {
				t.Errorf("not equal users after update, got %v, want %v", tt.u, *gotcheck)
			}
		})
	}
}

func TestPG_UpdatePassword(t *testing.T) {
	ctx, cancel := context.WithCancel(context.TODO())
	defer cancel()
	pg, err := repo.NewPG(ctx, cs, debug)
	if err != nil {
		t.Errorf("unexpected connect to db error, %s", err)
		return
	}
	defer pg.Close()
	// prepara data
	iu, err := pg.Store(ctx, mockNewUser(), "pass")
	if err != nil {
		t.Errorf("prepare users error, %s", err)
		return
	}
	u1 := *iu
	u1.Password = u1.HashPass(model.MakeSalt(model.SaltLen), "newPassw0rd1")
	u2 := *iu
	u2.Password = u2.HashPass(model.MakeSalt(model.SaltLen), "newPassw0rd2")
	tests := []struct {
		name    string
		u       model.User
		want    int64
		wantErr bool
	}{
		{"u1_pass1", u1, 1, false},
		{"u1_pass2", u2, 1, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := pg.UpdatePassword(ctx, tt.u)
			if (err != nil) != tt.wantErr {
				t.Errorf("PG.UpdatePassword() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("PG.UpdatePassword() = %v, want %v", got, tt.want)
			}
			gotcheck, err := pg.FindByID(ctx, tt.u.ID)
			if err != nil {
				t.Errorf("PG.FindByID() error = %v", err)
				return
			}
			if !reflect.DeepEqual(tt.u.Password, gotcheck.Password) {
				t.Errorf("not equal users.Password after update, got %v, want %v", tt.u, *gotcheck)
			}
		})
	}
}

func TestPG_Delete(t *testing.T) {
	ctx, cancel := context.WithCancel(context.TODO())
	defer cancel()
	pg, err := repo.NewPG(ctx, cs, debug)
	if err != nil {
		t.Errorf("unexpected connect to db error, %s", err)
		return
	}
	defer pg.Close()
	// prepara data
	iu, err := pg.Store(ctx, mockNewUser(), "pass")
	if err != nil {
		t.Errorf("prepare users error, %s", err)
		return
	}
	iu2, err := pg.Store(ctx, mockNewUser(), "pass2")
	if err != nil {
		t.Errorf("prepare users error, %s", err)
		return
	}
	tests := []struct {
		name    string
		u       model.User
		want    int64
		wantErr bool
	}{
		{"u1", *iu, 1, false},
		{"u2", *iu2, 1, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := pg.Delete(ctx, tt.u)
			if (err != nil) != tt.wantErr {
				t.Errorf("PG.Delete() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("PG.Delete() = %v, want %v", got, tt.want)
			}
			gotcheck, err := pg.FindByID(ctx, tt.u.ID)
			if err != nil && err != repo.ErrNotFound {
				t.Errorf("PG.FindByID() error = %v", err)
				return
			}
			if gotcheck != nil {
				t.Errorf("unexpected not nil user after delete %v", gotcheck)
			}
		})
	}
}

// helpers

func mockNewUser() model.User {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	user := "user-" + strconv.FormatInt(r.Int63(), 10)
	return model.User{
		Login: user,
		EMail: user + "@domain.com",
	}
}

func compareUsers(u1, u2 model.Users) (bool, string) {
	if len(u1) != len(u2) {
		return false, fmt.Sprintf("u1.len=%d != u2.len=%d", len(u1), len(u2))
	}
	eq := true
	diff := []string{}
	for i, u := range u1 {
		if u.Login != u2[i].Login {
			eq = false
			diff = append(diff, fmt.Sprintf("u1.Login=%s != u2.Login=%s", u.Login, u2[i].Login))
		}
		if u.EMail != u2[i].EMail {
			eq = false
			diff = append(diff, fmt.Sprintf("u1.EMail=%s != u2.EMail=%s", u.EMail, u2[i].EMail))
		}
	}
	return eq, strings.Join(diff, ", ")
}
