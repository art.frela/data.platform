/*
Package repo contains repository behaviods and some implementations
*/
package repo

import (
	"context"

	"gitlab.com/art.frela/data.platform/internal/users/model"

	"github.com/pkg/errors"
)

// contains common functions and helpers

const (
	RepoPostgres string = "postgres"
)

var (
	allowedProviders []string = []string{RepoPostgres}
)

// StorageInterface behavior of the user's repository
type StorageInterface interface {
	Store(ctx context.Context, u model.User, plainPassword string) (*model.User, error)
	FindByID(ctx context.Context, id string) (*model.User, error)
	FinfByLogin(ctx context.Context, login string) (*model.User, error)
	FinfByEMail(ctx context.Context, email string) (*model.User, error)
	FindAll(ctx context.Context, offset, limit int64) (model.Users, int64, error)
	Update(ctx context.Context, u model.User) (int64, error)
	UpdatePassword(ctx context.Context, u model.User) (int64, error)
	Delete(ctx context.Context, u model.User) (int64, error)
}

// New abstract fabric for repo
func New(ctx context.Context, provider, url string, debug bool) (StorageInterface, error) {
	switch provider {
	case RepoPostgres:
		return NewPG(ctx, url, debug)
	default:
		return nil, errors.Errorf("provider %s not supported, allowed only %v", provider, allowedProviders)
	}
}
