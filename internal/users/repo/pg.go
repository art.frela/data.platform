package repo

// pg.go contains postgres implementation of the repo interface

import (
	"context"
	"net"
	"time"

	"gitlab.com/art.frela/data.platform/internal/users/model"
	"gitlab.com/art.frela/data.platform/pkg/logging"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/log/zapadapter"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
)

const (
	pgMaxConns                 int32         = 8
	pgMinConns                 int32         = 4
	pgHealthCheckPeriod        time.Duration = 1 * time.Minute
	pgMaxConnLifetime          time.Duration = 24 * time.Hour
	pgMaxConnIdleTime          time.Duration = 30 * time.Minute
	pgConnConfigConnectTimeout time.Duration = 1 * time.Second
)

var (
	ErrNotFound error = errors.New("user not found")
)

// PG ...
type PG struct {
	db *pgxpool.Pool
}

// NewPG builder fog postgres implementation of the repo interface
func NewPG(ctx context.Context, url string, debug bool) (*PG, error) {
	logger := logging.FromContext(ctx)
	cfg, err := pgxpool.ParseConfig(url)
	if err != nil {
		return nil, errors.WithMessage(err, "parse url error")
	}
	// Pool соединений обязательно ограничивать сверху,
	// так как иначе есть потенциальная опасность превысить лимит соединений с базой.
	cfg.MaxConns = pgMaxConns
	cfg.MinConns = pgMinConns

	// HealthCheckPeriod - частота проверки работоспособности
	// соединения с Postgres
	cfg.HealthCheckPeriod = pgHealthCheckPeriod

	// MaxConnLifetime - сколько времени будет жить соединение.
	// Так как большого смысла удалять живые соединения нет,
	// можно устанавливать большие значения
	cfg.MaxConnLifetime = pgMaxConnLifetime

	// MaxConnIdleTime - время жизни неиспользуемого соединения,
	// если запросов не поступало, то соединение закроется.
	cfg.MaxConnIdleTime = pgMaxConnIdleTime

	// ConnectTimeout устанавливает ограничение по времени
	// на весь процесс установки соединения и аутентификации.
	cfg.ConnConfig.ConnectTimeout = pgConnConfigConnectTimeout

	// Лимиты в net.Dialer позволяют достичь предсказуемого
	// поведения в случае обрыва сети.
	cfg.ConnConfig.DialFunc = (&net.Dialer{
		KeepAlive: cfg.HealthCheckPeriod,
		// Timeout на установку соединения гарантирует,
		// что не будет зависаний при попытке установить соединение.
		Timeout: cfg.ConnConfig.ConnectTimeout,
	}).DialContext

	// logger
	if logger != nil && debug {
		cfg.ConnConfig.Logger = zapadapter.NewLogger(logger.Desugar())
	}
	pool, err := pgxpool.ConnectConfig(ctx, cfg)
	if err != nil {
		return nil, errors.WithMessage(err, "connect error")
	}
	return &PG{db: pool}, nil
}

func (pg *PG) Store(ctx context.Context, u model.User, plainPassword string) (*model.User, error) {
	const query string = `insert into public.users (login, email, password)
						values ($1,$2,$3)
						returning id, creator, created_at;`
	hash := u.HashPass(model.MakeSalt(model.SaltLen), plainPassword)
	var uid, creator string
	var createdAt time.Time
	err := pg.db.QueryRow(ctx, query, u.Login, u.EMail, hash).Scan(&uid, &creator, &createdAt)
	if err != nil {
		return nil, errors.WithMessage(err, "insert user failed")
	}
	if uid == "" {
		return nil, errors.Errorf("insert user failed, empty id")
	}
	u.ID = uid
	u.Creator = creator
	u.CreatedAt = createdAt
	return &u, nil
}

func (pg *PG) FindByID(ctx context.Context, id string) (*model.User, error) {
	const query string = `SELECT 
							id, login, email, "password", 
							creator, created_at, modifier, modified_at
						FROM public.users where id=$1;`
	var u model.User
	err := pg.db.QueryRow(ctx, query, id).Scan(&u.ID, &u.Login, &u.EMail, &u.Password,
		&u.Creator, &u.CreatedAt, &u.Modifier, &u.ModifiedAt)
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, ErrNotFound
		}
		return nil, errors.WithMessage(err, "fetch user failed")
	}
	return &u, nil
}

func (pg *PG) FinfByLogin(ctx context.Context, login string) (*model.User, error) {
	const query string = `SELECT 
							id, login, email, "password", 
							creator, created_at, modifier, modified_at
						FROM public.users where login=$1;`
	var u model.User
	err := pg.db.QueryRow(ctx, query, login).Scan(&u.ID, &u.Login, &u.EMail, &u.Password,
		&u.Creator, &u.CreatedAt, &u.Modifier, &u.ModifiedAt)
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, ErrNotFound
		}
		return nil, errors.WithMessage(err, "fetch user failed")
	}
	return &u, nil
}

func (pg *PG) FinfByEMail(ctx context.Context, email string) (*model.User, error) {
	const query string = `SELECT 
							id, login, email, "password", 
							creator, created_at, modifier, modified_at
						FROM public.users where email=$1;`
	var u model.User
	err := pg.db.QueryRow(ctx, query, email).Scan(&u.ID, &u.Login, &u.EMail, &u.Password,
		&u.Creator, &u.CreatedAt, &u.Modifier, &u.ModifiedAt)
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, ErrNotFound
		}
		return nil, errors.WithMessage(err, "fetch user failed")
	}
	return &u, nil
}

func (pg *PG) FindAll(ctx context.Context, offset, limit int64) (model.Users, int64, error) {
	const query string = `SELECT id, login, email, "password", 
							creator, created_at, modifier, modified_at
						FROM public.users
						order by email 
						offset $1
						limit $2;`
	const queryCount string = `select count(*) from public.users;`
	batch := &pgx.Batch{}
	batch.Queue(query, offset, limit)
	batch.Queue(queryCount)
	results := pg.db.SendBatch(ctx, batch)
	rows, err := results.Query()
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, 0, ErrNotFound
		}
		return nil, 0, errors.WithMessage(err, "batch.Query failed")
	}
	defer results.Close()
	defer rows.Close()
	users := make(model.Users, 0, limit)
	for rows.Next() {
		var u model.User
		err = rows.Scan(&u.ID, &u.Login, &u.EMail, &u.Password,
			&u.Creator, &u.CreatedAt, &u.Modifier, &u.ModifiedAt)
		if err != nil {
			return users, 0, errors.WithMessage(err, "rows.Scan failed")
		}
		users = append(users, u)
	}
	var count int64
	err = results.QueryRow().Scan(&count)
	if err != nil {
		return nil, count, errors.WithMessage(err, "results.QueryRow.Scan failed")
	}
	return users, count, nil
}

func (pg *PG) Update(ctx context.Context, u model.User) (int64, error) {
	const query string = `update public.users
						set login=$1, email=$2, modifier=current_user, modified_at=current_timestamp
						where id=$3`
	result, err := pg.db.Exec(ctx, query, u.Login, u.EMail, u.ID)
	if err != nil {
		return 0, errors.WithMessage(err, "pgxpool.Exec failed")
	}
	return result.RowsAffected(), nil
}

func (pg *PG) UpdatePassword(ctx context.Context, u model.User) (int64, error) {
	const query string = `update public.users
						set password=$1, modifier=current_user, modified_at=current_timestamp
						where id=$2`
	result, err := pg.db.Exec(ctx, query, u.Password, u.ID)
	if err != nil {
		return 0, errors.WithMessage(err, "pgxpool.Exec failed")
	}
	return result.RowsAffected(), nil
}

func (pg *PG) Delete(ctx context.Context, u model.User) (int64, error) {
	const query string = `delete from public.users where id=$1`
	result, err := pg.db.Exec(ctx, query, u.ID)
	if err != nil {
		return 0, errors.WithMessage(err, "pgxpool.Exec failed")
	}
	return result.RowsAffected(), nil
}

func (pg *PG) Close() {
	pg.db.Close()
}
