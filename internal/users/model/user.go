package model

import (
	"bytes"
	"crypto/rand"
	"time"

	"golang.org/x/crypto/argon2"
)

const (
	SaltLen int = 15
)

// User user's attributes
type User struct {
	ID       string `json:"id,omitempty"`
	Login    string `json:"login,omitempty"`
	EMail    string `json:"email,omitempty"`
	Password []byte `json:"-"`
	Metadata
}

type Users []User

type Metadata struct {
	Creator    string     `json:"creator,omitempty"`
	CreatedAt  time.Time  `json:"created_at,omitempty"`
	Modifier   *string    `json:"modifier,omitempty"`
	ModifiedAt *time.Time `json:"modified_at,omitempty"`
}

// HashPass calculate password hash with salt
func (u *User) HashPass(salt []byte, plainPassword string) []byte {
	hashedPass := argon2.IDKey([]byte(plainPassword), salt, 1, 64*1024, 4, 32)
	return append(salt, hashedPass...)
}

// CheckPass compare hash and string password
func (u *User) CheckPass(passHash []byte, plainPassword string) bool {
	salt := make([]byte, SaltLen)
	copy(salt, passHash)
	userHash := u.HashPass(salt, plainPassword)
	return bytes.Equal(userHash, passHash)
}

// helpers

// nolint:errcheck
func MakeSalt(n int) []byte {
	salt := make([]byte, n)
	rand.Read(salt)
	return salt
}
