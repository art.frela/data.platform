package model

import (
	"crypto/rand"
	"testing"
)

var (
	salt          []byte = make([]byte, SaltLen)
	plainPassword string = "password"
)

func TestHashPass(t *testing.T) {
	tests := []string{"pass1", "love", "$Tr0nGP@$$w0rd123", "____PASS____", "empty--", "        "}
	u := &User{}
	for _, plainPass := range tests {
		t.Run(plainPass, func(t *testing.T) {
			rand.Read(salt)
			passBts := u.HashPass(salt, plainPass)
			if !u.CheckPass(passBts, plainPass) {
				t.Error("unexpected not equal hashs passwords")
			}
		})
	}
}

func BenchmarkHashPass(b *testing.B) {
	rand.Read(salt)
	u := &User{}
	for i := 0; i < b.N; i++ {
		u.HashPass(salt, plainPassword)
	}
}
