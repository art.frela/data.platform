// Package config implements common config methods
package config

import (
	"strings"

	"github.com/spf13/viper"
)

type Config struct {
	APP struct {
		Name string
	}
	HTTPD struct {
		Port string
	}
	DB struct {
		Provider string
		URL      string
	}
	LOG struct {
		Level  string
		Format string
	}
}

var Defaults = map[string]interface{}{
	"app": map[string]string{
		"name": "otushwX",
	},
	"httpd": map[string]string{
		"host": "",
		"port": "8000",
	},
	"db": map[string]string{
		"url":      "postgres://postgres:secretpassword@localhost:5432/users?sslmode=disable ",
		"provider": "postgres",
	},
	"log": map[string]string{
		"level":  "info",
		"format": "console",
	},
}

// Read makes config by reading config file, environment variables,
// if some parameter not passed default value will be used
//
// if pass config structure will be unmarshal to them
func Read(appName, envPrefix string, defaults map[string]interface{}, cfg interface{}) (*viper.Viper, error) {
	v := viper.New()
	for key, value := range defaults {
		v.SetDefault(key, value)
	}
	v.SetConfigName(appName)
	v.SetConfigType("yaml")
	v.AddConfigPath(".")                     // search in the localpath
	v.AddConfigPath("/etc/" + appName + "/") // search in the /etc/appname/...
	v.SetEnvPrefix(envPrefix)                // for multi service
	v.AutomaticEnv()
	v.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	err := v.ReadInConfig()
	if err != nil {
		return v, err
	}
	if cfg != nil {
		err := v.Unmarshal(&cfg)
		if err != nil {
			return v, err
		}
	}
	return v, nil
}
