# Task homework 3 Prometheus. Grafana

Инструментировать сервис из прошлого задания метриками в формате Prometheus с помощью библиотеки для вашего фреймворка и ЯП.

Сделать дашборд в Графане, в котором были бы метрики с разбивкой по API методам:

1. Latency (response time) с квантилями по 0.5, 0.95, 0.99, max
2. RPS
3. Error Rate - количество 500ых ответов

Добавить в дашборд графики с метрикам в целом по сервису, взятые с nginx-ingress-controller:

1. Latency (response time) с квантилями по 0.5, 0.95, 0.99, max
2. RPS
3. Error Rate - количество 500ых ответов

Настроить алертинг в графане на Error Rate и Latency.

На выходе должно быть:

0) скриншоты дашборды с графиками в момент стресс-тестирования сервиса. Например, после 5-10 минут нагрузки.
1) json-дашборды.


Задание со звездочкой (+5 баллов)
Используя существующие системные метрики из кубернетеса, добавить на дашборд графики с метриками:

1. Потребление подами приложения памяти
2. Потребление подами приолжения CPU

Инструментировать базу данных с помощью экспортера для prometheus для этой БД.
Добавить в общий дашборд графики с метриками работы БД.

>Рекомендуем сдать до: 10.02.2021

## Solution

- Добавил в приложение одну метрику через middlware `http_duration_milliseconds` типа Histogram и из нее вычислил все остальные показатели RPS, Latency + Error rate  
- раскатил так же как и в лекции прилодение в один namespace, а prometheus c nginx-ingress-controller-ом в другой, все заработало )
- из коробки чарта `prometheus-community/kube-prometheus-stack` была графана с дашбордами для kubernetes-a - графики для pod скопировал оттуда  
- в чарте БД `bitnami/postgresql` есть параметр, котрый активирует запуск экспортера для бд, активировал его, а дашборд для postgres-a нагуглил и уже из него вставил графики для БД  
- ну а настроить графану это дело техники, уже знаком с ней

### Скриншоты

> метрики приложения + через nginx-ingress-controller
![application](../../../asserts/img/otushw3_app.png)

> метрики БД (некоторые)
![db](../../../asserts/img/otushw3_db.png)
> метрики под приложения и бд
![pod](../../../asserts/img/otushw3_pod.png)

[ссылка на json дашборда](https://gitlab.com/art.frela/data.platform/-/blob/master/deployments/otus/hw3/prom/application.dashboard.json)

## Кое какие детали

### Prometheus through HELM

```bash
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update

# k - alias to kubectl
k create namespace monitoring
helm install prom prometheus-community/kube-prometheus-stack -f deployments/otus/hw3/prom/prometheus.yaml -n monitoring
helm install nginx ingress-nginx/ingress-nginx -f deployments/otus/hw3/prom/nginx-ingress.yaml --atomic -n monitoring

helm install userpg bitnami/postgresql -f deployments/otus/hw3/db/myvalues.yaml -n otushw3
helm install otushw3 ./deployments/otus/hw3/users -n otushw3
```

### Нагрузочное тестирование через wrk

> Запуск `wrk -t4 -c10 -d10m -s scripts/otus/hw3/wrk.lua http://arch.homework`

### PromQL из лекции

```promql
# rps app
sum (rate(http_duration_milliseconds_count[1m]))
# latency
histogram_quantile(0.95, sum by (le) (rate(http_duration_milliseconds_bucket[1m])))
# error rate
sum (rate(http_duration_milliseconds_count{code=~"5.+"}[1m]))

# nginx
# rps
rate(nginx_ingress_controller_request_duration_seconds_count[1m])
# latency
histogram_quantile(0.95, sum by (le) (rate(nginx_ingress_controller_request_duration_seconds_bucket[1m])))
# error rate
sum(rate(nginx_ingress_controller_request_duration_seconds_count{status=~"5.+"}[1m]))
```